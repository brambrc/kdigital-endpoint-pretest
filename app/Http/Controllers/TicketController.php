<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Illuminate\Support\Str;
Use Exception;

class TicketController extends Controller
{
    
    public function index()
    {
        $data = Ticket::get()->take(5);
        return response()->json([
            'status' => 'successs',
            'data'   => $data,
        ]);

    }


    public function store(Request $request)
    {
        try{
            $ticket_id = Str::random(6);
            $data = Ticket::create($request->all() + ['ticket_number' => $ticket_id, 'status' => 'Open']);
            return response()->json([
                'status' => 'success',
                'msg'    => 'Ticket Successfully Submited !',
            ]);
        } catch (\Exception $exception){
            return response()->json([
                'status' => 'error',
                'msg'    => 'Something really wrong',
            ]);
        }
    }

    
    public function reply(Request $request)
    {
        try{
            $find = Ticket::where('ticket_number', $request->ticket_number)->first();
            $ruler = "
                        ---------------reply---------------
                     ";
            $find->message = $ruler.$request->message;
            $find->status = "Answered";
            $find->save();
            return response()->json([
                'status' => 'success',
                'msg'    => 'Ticket Successfully Replied !',
            ]);

        } catch(\Exception $exception){
            return response()->json([
                'status' => 'error',
                'msg'    => 'Something really wrong',
            ]);
        }
    }

    
    public function closeTicket($id)
    {
        try {
            $close = Ticket::where('ticket_number', $id)->first();
            $close->status = "Closed";
            $close->save();
            return response()->json([
                'status' => 'success',
                'msg'    => 'Ticket Successfully Closed !',
            ]);
        } catch(\Exception $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Something really wrong',
            ]);
        }
    }


    public function destroy($id)
    {
        try {
            $close = Ticket::findOrFail($id)->first();
            $close->delete();
            return response()->json([
                'status' => 'success',
                'msg'    => 'Ticket Successfully Deleted !',
            ]);
        } catch(\Exception $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Something really wrong',
            ]);
        }
    }
}
