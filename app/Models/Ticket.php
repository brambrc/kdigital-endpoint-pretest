<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\Uuids;

class Ticket extends Model
{

    use Uuids;

    protected $table = 'ticket';

    protected $fillable = [
        'ticket_number', 'subject', 'message', 'status', 'priority',
    ];


}
