<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How To Run it on local machine?

Important Notes : Since this is a Laravel 8 project, you must had php 8.0 or later installed on your machine !.
1. Clone the repos to your local development
2. get into the project directories in your local machine, then try installing the composer using "composer install" or "composer update". (Please ignore the ").
3. After that, please open your database and create new database.
4. Open and rename ".env.example" file to ".env"  on project directories and fill the database name section with database you just created before.
5. Back to the terminal / cmd, and try write "php artisan migrate" to generate tables. (Please ignore the ").
6. After that please type "php artisan serve" on the terminal / cmd to start the server. (Please ignore the ").

## How To Test?

Great now lets try to test the API. First prepare the postman Apps.


### Getall Data

URL : http://127.0.0.1:8000/index <br>
Method : GET <br>
notes: no params / body required. <br>

Still empty? why not create some record first?

### Generate Token
Some of the POST method required token headers due to laravel security concern, to obtain it please use this API:

URL : 127.0.0.1:8000/token <br>
Method : GET <br>
notes: no params, headers, and body required.

### Store Tickets

URL: http://127.0.0.1:8000/submitTickets <br>
Method : POST <br>
Headers : X-CSRF-TOKEN = please place the generated token here. <br>
Body : subject => text, message => text, priority => [Low, High, Medium]  (please choose only one enum for priority values).

### Reply Tickets

URL: http://127.0.0.1:8000/reply <br>
Method : POST <br>
Headers : X-CSRF-TOKEN = please place the generated token here. <br>
Body : ticket_number => please fill the value with existing ticket number, message => text,

### Closed Tickets

URL : http://127.0.0.1:8000/closeTicket/please_put_ticket_number_here <br>
Method : GET <br>
notes: no body and headers required. <br>

### Delete Tickets

URL: http://127.0.0.1:8000/delete/please_put_uuid_here <br>
Method : GET <br>
notes: no body and headers required.

Confused?

Relax, i also prepared JSON Postman Template to make it easier to test
The file are in the project directories named kdigital_postman_collection.json


## Can't deploy in your local machine?

I Also prepared the hosted endpoint here <a href="https://kdigital-endpoint.bracmatya.id">kdigital-endpoint.bracmatya.id</a> <br>
And JSON Postman Template for hosted endpoint named kdigital_postman_collection_hosted.json in project directories

