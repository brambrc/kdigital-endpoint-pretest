<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/token', function () {
    return csrf_token(); 
});
Route::get('/tickets', [TicketController::class, 'index']);
Route::post('/submitTickets', [TicketController::class, 'store']);
Route::post('/reply', [TicketController::class, 'reply']);
Route::get('/closeTicket/{ticket_number}', [TicketController::class, 'closeTicket']);
Route::get('/delete/{id}', [TicketController::class, 'destroy']);


Route::get('/', function () {
    return view('welcome');
});


